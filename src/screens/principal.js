import React from 'react';
import { Image, Dimensions } from 'react-native';
import TarefaService from '../services/tarefa';
import Comentario from '../components/comentario';
import { NormalText } from '../components/text';
import styled from 'styled-components';
import Ligar from '../components/button/ligar';
import Servicos from '../components/button/servicos';
import Endereco from '../components/button/endereco';
import Comentarios from '../components/button/comentarios';
import Favorito from '../components/button/favorito';

export default class Principal extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            item: ''
        }

    }

    componentDidMount() {
        const { navigation } = this.props;
        TarefaService.getById(navigation.getParam('item'))
            .then(response => {
                this.setState({
                    item: response.data
                })
            })
            .catch(() => {
                //message to user
            })
    }

    render() {
        const dimensions = Dimensions.get('window').width;
        return (
            <Container>
                <Image source={{ uri: this.state.item.urlLogo }} style={{ width: dimensions, height: 200 }} />
                <TituloFoto>
                    <TituloText>{this.state.item.titulo}</TituloText>
                    <Image source={{ uri: this.state.item.urlFoto }} style={{ width: 60, height: 60 }} />
                </TituloFoto>
                <TextoButtonsContainer>
                    <Actions>
                        <Ligar numero={this.state.item.telefone} />
                        <Servicos {...this.props} />
                        <Endereco endereco={this.state.item.endereco} />
                        <Comentarios action={this.scrollToRow} />
                        <Favorito />
                    </Actions>
                    <Texto>
                        <NormalText>{this.state.item.texto}</NormalText>
                    </Texto>
                </TextoButtonsContainer>
                {this.state.item.comentarios.map(comentario => <Comentario comentario={comentario} key={comentario.urlFoto} />)}
            </Container>
        )
    }
}

const TituloFoto = styled.View`
    flex-direction: row;
    justify-content: space-between;
`;

const TituloText = styled.Text`
    color: #CC8A19;
    font-size: 22px;
    padding: 15px;
`

const TextoButtonsContainer = styled.View`
    background-color: white;
    padding: 25px;
`;


const Container = styled.ScrollView`
    background-color: #f5f5f5
`;

const Actions = styled.View`
    flex-direction: row;
    justify-content: space-between;
`;

const Texto = styled.View`
    margin-top: 10px;
`;



