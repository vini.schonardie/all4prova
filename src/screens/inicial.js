import React from 'react';
import ListaTarefa from '../components/listaTarefa';
import TarefaService from '../services/tarefa';
import { View } from 'react-native';

export default class Inicial extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            lista: []
        }
    }

    componentDidMount() {
        TarefaService.list()
            .then(response => {
                alert(response);
                this.setState({
                    lista: response.data.lista
                })
            })
            .catch(() => {
                //message to user
            })
    }

    render() {
        return (
            <View>
                <ListaTarefa lista={this.state.lista} {...this.props}/>
            </View>
        )
    }

}