import axios from 'axios';

const HttpClient = axios.create({
  baseURL: 'http://dev.4all.com:3003'
});

export default HttpClient;
