import HttpClient from './http';

const list = () => HttpClient.get('/tarefa');
const getById = id => HttpClient.get(`/tarefa/${id}`);

export default { list, getById };
