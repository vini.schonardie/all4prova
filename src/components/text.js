import styled from 'styled-components';

const NormalText = styled.Text`
    color: #CC8A19;
    font-size: 14px;
`;

const SmallText = styled.Text`
    color: #CC8A19;
    font-size: 10px;
`;

export { NormalText, SmallText }