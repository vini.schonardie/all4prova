import React from 'react';
import styled from 'styled-components';
import { Image } from 'react-native';
import { SmallText } from '../text';
import call from 'react-native-phone-call'

export default class Ligar extends React.Component {
    constructor(props) {
        super(props);
    }

    ligar() {
        const args = {
            number: this.props.numero, // String value with the number to call
            prompt: false // Optional boolean property. Determines if the user should be prompt prior to the call 
        }
        call(args).catch(console.error)
    }

    render() {
        return (
            <Container onPress={this.ligar.bind(this)}>
                <Image source={require('../../images/ligar.png')} style={{ width: 30, height: 30 }} />
                <SmallText>Ligar</SmallText>
            </Container>
        )
    }
}

const Container = styled.TouchableOpacity`
    align-items: center;
`;