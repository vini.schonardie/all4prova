import React from 'react';
import styled from 'styled-components';
import { Image } from 'react-native';
import { SmallText } from '../text';

const Comentarios = () => {
    return (
        <Container>
            <Image source={require('../../images/comentarios.png')} style={{ width: 30, height: 30 }} />
            <SmallText>Comentarios</SmallText>
        </Container>
    )
}

const Container = styled.View`
    align-items: center;
`;

export default Comentarios;

