import React from 'react';
import styled from 'styled-components';
import { Image } from 'react-native';
import { SmallText, NormalText } from '../text';
import Dialog, { DialogContent } from 'react-native-popup-dialog';


export default class Endereco extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showPopup: false
        }
    }

    showPopup() {
        this.setState({
            showPopup: !this.state.showPopup
        })
    }

    render() {
        return (
            <Container onPress={() => { this.setState({ showPopup: true }) }}>
                <Image source={require('../../images/endereco.png')} style={{ width: 30, height: 30 }} />
                <SmallText>Endereço</SmallText>
                <Dialog
                    visible={this.state.showPopup}
                    onTouchOutside={() => {
                        this.setState({ showPopup: false });
                    }}
                >
                    <DialogContent>
                        <NormalText>{this.props.endereco}</NormalText>
                    </DialogContent>
                </Dialog>
            </Container>
        )
    }
}

const Container = styled.TouchableOpacity`
            align-items: center;
        `;
