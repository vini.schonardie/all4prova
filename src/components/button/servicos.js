import React from 'react';
import styled from 'styled-components';
import { Image } from 'react-native';
import { SmallText } from '../text';

export default class Servicos extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Container onPress={() => this.props.navigation.navigate('Servicos')}>
                <Image source={require('../../images/servicos.png')} style={{ width: 30, height: 30 }} />
                <SmallText>Servicos</SmallText>
            </Container>
        )
    }
}

const Container = styled.TouchableOpacity`
    align-items: center;
`;
