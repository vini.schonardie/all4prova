import React from 'react';
import styled from 'styled-components';
import { Image } from 'react-native';
import { SmallText } from '../text';

const Favorito = () => {
    return (
        <Container>
            <Image source={require('../../images/favoritos.png')} style={{ width: 30, height: 30 }} />
            <SmallText>Favoritos</SmallText>
        </Container>
    )
}

const Container = styled.View`
    align-items: center;
`;

export default Favorito;

