import React from 'react';
import { Image } from 'react-native';
import { NormalText } from './text';
import styled from 'styled-components';

export default class Comentario extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <Container>
                <ImageContainer>
                    <Image source={{ uri: this.props.comentario.urlFoto }} style={{ width: 60, height: 60 }} />
                </ImageContainer>
                <TextContainer>
                    <Avaliacao>
                        <NormalText>{this.props.comentario.nome}</NormalText>
                        <Favoritos>
                            {this.props.comentario.nota >= 1 &&
                                <Image source={require('../images/favoritos.png')} style={{ width: 20, height: 20 }} />
                            }
                            {this.props.comentario.nota >= 2 &&
                                <Image source={require('../images/favoritos.png')} style={{ width: 20, height: 20 }} />
                            }
                            {this.props.comentario.nota >= 3 &&
                                <Image source={require('../images/favoritos.png')} style={{ width: 20, height: 20 }} />
                            }
                            {this.props.comentario.nota >= 4 &&
                                <Image source={require('../images/favoritos.png')} style={{ width: 20, height: 20 }} />
                            }
                            {this.props.comentario.nota >= 5 &&
                                <Image source={require('../images/favoritos.png')} style={{ width: 20, height: 20 }} />
                            }
                        </Favoritos>
                    </Avaliacao>
                    <NormalText>{this.props.comentario.titulo}</NormalText>
                    <NormalText>{this.props.comentario.comentario}</NormalText>
                </TextContainer>
            </Container>
        )
    }
}

const Container = styled.View`
    flex-direction: row;
    background-color: white;
    padding: 5px;
    margin-top: 5px;
    width: 100%;
`;

const Favoritos = styled.View`
    flex-direction: row;
`;

const Avaliacao = styled.View`
    flex-direction: row;
    justify-content: space-between;
`;

const ImageContainer = styled.View`
    width: 20%;
`;

const TextContainer = styled.View`
    flex-direction: column;
    width: 80%;
`;