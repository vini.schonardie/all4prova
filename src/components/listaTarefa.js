import React from 'react';
import { View, FlatList } from 'react-native';
import { NormalText } from './text';
import styled from 'styled-components';

export default class ListaTarefa extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <View>
                <FlatList
                    data={this.props.lista}
                    renderItem={({ item }) =>
                        <Container onPress={() => this.props.navigation.navigate('Principal', { item: item })}>
                            <NormalText>{item}</NormalText>
                        </Container>
                    }
                    keyExtractor={(item) => item}
                />
            </View>
        )
    }
}

const Container = styled.TouchableOpacity`
    padding: 5px;
    border: 1px solid #CC8A19;
    margin-top: 2px;
`;