import { createStackNavigator, createAppContainer } from 'react-navigation';
import InicialScreen from './screens/inicial';
import PrincipalScreen from './screens/principal';
import ServicosScreen from './screens/servicos';

const StackNavigator = createStackNavigator(
    {
        Inicial: {
            screen: InicialScreen,
            navigationOptions: () => ({
                title: 'Inicial'
            })
        },
        Principal: {
            screen: PrincipalScreen,
            navigationOptions: () => ({
                title: 'Principal'
            })
        },
        Servicos: {
            screen: ServicosScreen,
            navigationOptions: () => ({
                title: 'Servicos'
            })
        },
    },
    {
        initialRouteName: 'Inicial',
        headerLayoutPreset: 'center',
        defaultNavigationOptions: {
            headerStyle: {
                backgroundColor: '#CC8A19'
            },
            headerTitleStyle: {
                color: 'white'
            }
        },
    }
);

const Navigator = createAppContainer(StackNavigator);

export default Navigator;